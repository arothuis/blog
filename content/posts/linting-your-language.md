---
title: "Linting your Language"
subtitle: "Check your blog for inconsiderate writing using Alex"
date: "2018-09-15"
tags: ["meta", "empathy", "ethics"]
summary: "
    It is no secret the software industry
    has issues regarding diversity and inclusivity.
    The use of language can play a role 
    in creating an atmosphere that is welcoming to everyone 
    -- or, at the very least, not uninviting.


    For the purpose of shaping a 
    welcoming community through language
    and not alienating anyone, it 
    is necessary to be mindful 
    (or reminded) of sensitivities, 
    especially of those not felt by the author or 
    voiced by the people in the author's bubble.


    `Alex` is a tool that can help with this.
"
---
<!-- alex disable obvious whitelist blacklist taboo angry assassinations killer racist master master-slave slave handyman-handywoman -->

It is no secret the software industry
has issues regarding 
[diversity and inclusivity][diversity-and-inclusivity].
The [use of language][bias-language] can play a role 
in creating an atmosphere that is welcoming to everyone 
-- or, at the very least, not uninviting.

This is why some, like [André Staltz][andrestaltz-words]
(creator of Cycle.js) 
and [David Heinemeier Hansson][dhh-words] 
(founder of Ruby on Rails) 
propose to change some of the words, used in our industry:

> Software development still has a lot of terminology with a racist or patriarchal background. Let's change that.
>
> - Blacklist ➡ Denylist
> - Whitelist ➡ Allowlist
> - Killer app ➡ Beloved app
> - Master/slave ➡ primary/replica
>

This is not a new phenomenon.
In 2014, the Drupal team decided to [change][drupal-words] 
the use of master/slave terminology to
primary/replica. The Django team opted to do the same:
first to [leader/follower][django-words-1] and later to [primary/replica][django-words-2]. And so did [CouchDB][couchdb-words].
More recently,
[Python][python-words] and [Redis][redis-words] 
also got rid of their master/slave
vocabulary -- albeit 
not [without struggle][redis-struggle].

Likewise, every now and again,
the term "craftsman" or "craftsmanship" is debated.
[Sarah Mei][sarah-mei-words] 
(founder of Rails Bridge and architect at Salesforce) is often
involved:
not necessarily discussing the merits of the 
"software craftsmanship" movement, but because
the term craftsman could be seen as unwelcoming
(or, as some put it, oppressive) to
those who do not identify as male.

Although I have [voiced my concerns on Twitter][arothuis-words]
regarding [general embargoes][sarah-mei-words2] earlier this year,
I think it is beneficial for cultures
to review the language used every day.

For the purpose of shaping a 
welcoming community through language
and not alienating anyone, it 
is necessary to be mindful 
and reminded of sensitivities -- 
especially of those not felt by the author or 
voiced by the people in the author's bubble.

# The "right" choice of words?
That being said, I think we should be wary of 
a [chilling effect][chilling-effect] of sorts
by automatically attacking, dismissing or silencing those 
who utter words or phrases that rattle us without
considering context or intent. 
By definition, the [connotation][connotation] of a
word differs per culture, society 
or social group and
is subject to [change over time][semantic-change]. 
That is why declaring
certain words as taboo, offensive or harmful 
and calling it a day is
ill-advised and unrealistic 
--- especially with regards to
intercultural and international settings.

Speak up when you think
someone is expressing themselves 
in a poor or potential harmful manner, 
but do so in a way that is not harmful itself.
Be aware of the fact that a public accusation has consequences:
Twitter crusades, angry mobs, witch hunts and character 
assassinations.
Even if the accusation was later proven to be unfounded, 
it could disproportionately damage someone's career forever.
That is why it is advisable to presume innocence, 
keep an open mind, maintain a constructive dialogue and
dilligently apply the principle of least harm. 
Reach out in private before escalating the matter 
in a public forum and try to 
provide evidence and examples for improvement.

The "right" choice of words, 
the boundaries between 
[euphemism][euphemism] and [dysphemism][dysphemism],
[approbative][approbative] and [pejorative][pejorative],
should not be determined by blanket statements but
be evaluated on a case-by-case basis. 
They should evolve through dialogue and mutual respect.
In my opinion, warnings for inconsiderate writing 
should therefore not unquestionably lead to self-censorship
but be regarded as a starting point 
for understanding, empathy and conversation.
I believe `Alex` can help with this.

# The tool
For obvious reasons, I think
the best thing about this tool is its name: `alex`. 😉
Joking aside, [alex][alex-project] is a tool made by
[Titus Worm][titus-worm] designed to check your writing
for possible insensitivities.

Alex utilizes [unified][unified], 
[retext][retext] and [remark][remark] 
for processing text and markdown.
The dictionaries at
[retext-equality][retext-equality]
and [retext-profanities][retext-profanities]
are used as a corpus of harmful language.
If you think another word should be included,
you could create a pull request 
in either of those repositories. 
If you are ever in need of another 
text-processing tool in JavaScript -- for 
instance for determining the readability of an 
article, detecting the sentiment of a text or 
checking for repeated words, passive voice or 
redundant acronyms -- 
you may want to check out 
the [other retext libraries][retext-other].

Alex can be integrated with 
[several JS-based programs][alex-integrations]. 
There are integrations for 
Atom, Sublime and VSCode,
but also for Slack and Ember.

# Exceptions
Sometimes, a word is *not* used inconsiderately 
or inappropriately. 
One could, for instance, 
refer to the word as-is,
quote someone who used this word precisely or
use a word in a non-harmful context.
Likewise, one could respectfully disagree with the
tool's assessment.

Even though `alex` is configured to recognize
whether a word is referred to in a literal sense
or not, a `.alexrc` file can
be added, an `alex` field can be added in `package.json`
or (partial) exception clauses 
can be inserted into the source document.

If I wanted to silence the word "execution" 
in the entire document, this can be done by
prepending the following comment to your text,
markdown or otherwise:

```
<!-- alex disable execution -->
```

You could decide to re-enable the check later in the file.
Alternatively, you can disable the check for a
specific subsequent block of text, 
such as a paragraph, quote or list:

```
<!-- alex ignore execution pop -->
```

As you can see, you can control 
multiple messages in a single statement.

# Continuous integration
The blog you are reading right now is built
using a static site generator called [Hugo][hugo].
I have [configured GitLab CI][gitlab-ci-config]
to let Hugo generate HTML upon each change on master.
The results are then hosted via [GitLab pages][gitlab-pages].

As with (other) programming projects,
we can configure GitLab CI to verify
whether our added content integrates with our
existing codebase before deploying it. 
Tests verify intended behaviour,
linters unify code style and
static analysis tools monitor code quality.
We can setup `alex` to check our writing as well:

```yml
stages:
  - check
  - deploy

check:
  stage: check
  image: node:8
  script:
    - npm install -g alex
    - alex content

pages:
  # ...
```

Both the task as the stage are called `check`.
For clarity, we would want to change this 
if we add another check.
This task is configured to run on every branch
and, if applicable, before the `deploy` stage.
A node docker image is booted, and `alex` is
installed and run against the `content` directory.

If there is questionable language in my writing,
`alex` shows the warnings in the build log
and throws a non-zero exit code.
This stops the build and halts deployment.

# In conclusion
Inclusivity and the use of language
is continuously 
a hot topic in society in general and 
software development in particular.
Regardless of whether you think certain
phrases are harmful or not, a tool like
`alex` helps you identify potential issues
with your writing you may have overlooked
and invites you to think about their impact.
You can then decide to change your wording
or to actively ignore the warning.

Detect issues in your code editor, in Slack
or before publishing articles. Adding `alex` to
your continuous integration pipeline can
prevent deployment of documents or posts
containing inconsiderate writing.

[drupal-words]: https://www.drupal.org/node/2275877
[django-words-1]: https://github.com/django/django/pull/2692
[django-words-2]: https://github.com/django/django/pull/2694
[couchdb-words]: https://issues.apache.org/jira/browse/COUCHDB-2248
[python-words]: https://bugs.python.org/issue34605
[redis-words]: https://github.com/antirez/redis/issues/3185
[redis-struggle]: http://antirez.com/news/122
[andrestaltz-words]: https://twitter.com/andrestaltz/status/1030200563802230786
[dhh-words]: https://twitter.com/dhh/status/1032050325513940992
[sarah-mei-words]: https://twitter.com/sarahmei/status/990268357269061632
[sarah-mei-words2]: https://twitter.com/sarahmei/status/990267662088290304
[arothuis-words]: https://twitter.com/ARothuis/status/992038790691721218
[diversity-and-inclusivity]: http://www.businessinsider.com/infographic-tech-diversity-companies-compared-2017-8?international=true&r=US&IR=T
[bias-language]: http://communication.oxfordre.com/view/10.1093/acrefore/9780190228613.001.0001/acrefore-9780190228613-e-470
[chilling-effect]: https://en.wikipedia.org/wiki/Chilling_effect
[connotation]: https://en.wikipedia.org/wiki/Connotation
[semantic-change]: https://en.wikipedia.org/wiki/Semantic_change
[euphemism]: https://en.wikipedia.org/wiki/Euphemism
[dysphemism]: https://en.wikipedia.org/wiki/Dysphemism
[approbative]: https://en.wikipedia.org/wiki/Approbative
[pejorative]: https://en.wikipedia.org/wiki/Pejorative
[alex-project]: http://alexjs.com/
[alex-integrations]: http://alexjs.com/#integrations
[unified]: https://unifiedjs.github.io/
[remark]: https://github.com/remarkjs/remark
[retext]: https://github.com/retextjs/retext
[retext-profanities]: https://github.com/retextjs/retext-profanities
[retext-equality]: https://github.com/retextjs/retext-equality
[retext-other]: https://github.com/retextjs
[hugo]: https://gohugo.io/
[gitlab-pages]: https://github.com/retextjs/retext 
[gitlab-ci-config]: https://gitlab.com/arothuis/blog/blob/d7b399581d61b52da2e1bf1f40153cf21459b1b5/.gitlab-ci.yml
[titus-worm]: http://wooorm.com/