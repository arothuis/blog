---
title: "About"
hideMetadata: true
tags: []
---

<!-- alex disable just masters -->

<figure>
    <img src="/img/arothuis.jpg" width="250" class="round">
</figure>

# Who am I?
My name is Alex Rothuis, a software engineer who 
appreciates well-crafted code regardless of programming language.
Currently, I work as a Freelance Software Developer, Consultant and Trainer
under the name Chamber of Code.
Before this, I worked as a Software Developer for companies 
like Ibuildings and NEP 
and I was a Software Development lecturer 
at the [HU University of Applied Sciences][hu].

I value high-quality, empathic and ethical software,
built using a principled approach, a thorough process
and a suitable architectural foundation and
continuously verified through (automated) tests.

While in law school, 
software development captured my interest
because I wanted to make a legal knowledge system.
This motivated me to learn as much about
software quality, architecture and development
as possible.

I finished my legal masters in both 
Dutch Civil Law and Information and Technology Law, 
but decided to pursue a career in software.
I am a full-time software engineer ---
if I'm not on the job, I often work on a side-project,
watch a conference talk or read a book, article
or blog post about software.

Interested in meeting, sparring or just talking?
[LinkedIn][linkedin] and 
[Twitter][twitter]
are preferred ways of reaching out to me.

Articles are updated on a regular basis to add
nuance, more information or recent insights.
The repository, containing a commit history
of articles, can be found on [GitLab][repo].

_This site is pre-rendered using [Hugo][hugo], 
a static site generator written in Go._

[hu]: https://www.international.hu.nl/
[linkedin]: https://www.linkedin.com/in/alexrothuis/
[twitter]: https://twitter.com/ARothuis
[repo]: https://gitlab.com/arothuis/blog
[hugo]: https://gohugo.io/