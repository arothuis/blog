---
title: "Getting Started with Unit Testing"
subtitle: "Why, What and How"
date: "2019-07-10"
series: "software-quality"
tags: ["software quality", "testing", "python", "javascript", "java"]
summary: "
    Automated testing is useful for gaining confidence in the
    functionality of the systems we build. Test cases
    can not only indicate whether changes will break functionality 
    or not, they can also help drive the (structural) design of
    the software.
    
    
    There are various layers on which automated testing can be applied
    and there are several methods of testing software.
    In this article we will take an introductory look 
    at *unit testing*: why it is useful, what it is and how
    to get started.
"
---
<!-- alex disable execution executed -->

# Why is Unit Testing Useful?
A common way of thinking about testing is 
that tests verify the behaviours
expressed by a system under test. This
is the first reason why unit testing is useful:
runtime *verification* of functionality.

In addition to that, our tests form a *documentation*
on how a certain unit of code behaves:
by spelling out test cases we can describe the
capabilities, assumptions and pitfalls of our
systems in an *example-based and executable* fashion.
Test cases describe what a piece of code does and how
it can and/or should be used.

Not only can tests be used to *describe* implemented
functionality, they can be used to *prescribe* desired
features or bugs to prevent. This is most noticable
when writing tests first. Test-first programming leads
to a software design that *is* testable. Typically,
this is code that is well-factored, cohesive and decoupled. 
This is at the heart of Test-Driven Development (TDD): 
development, driven by tests.
Or, [more generally speaking][example-guided-development-1], 
[Example-Guided Development][example-guided-development-2].
It makes sense
that a more decoupled design can be achieved when writing 
the implementation with tests in mind 
even when not doing full-blown test-first
TDD.

# What is Unit Testing?
* testing of the building blocks of our application

When is a Unit... a Unit?
* SoC
* SRP
* Component? Integration?


## Typical Test Automation Ingredients
* system under test
* test driver
* collaborators / test doubles


# How to Unit Test?
Using the right tools is a good start,
but there are some [principles][test-principles]
to increase the quality of our tests:

1. Strive for readability
1. Follow a logical pattern
1. Keep test cases small
1. Be specific
1. Refactor our tests

## Strive for Readability
A common way of thinking about testing is 
that tests verify the behaviours
expressed by a unit under test.
However, one could turn it around and say
that tests are the specification for implemented
behaviours.

## Follow a Logical Pattern
In order to make a test easier to read,
follow a logical pattern in each test.
This is quite similar to writing
any other function or method, which also
have a common structure to them.
In a typical implementation, a
function call will have a common structure 
of three parts, reminiscent of the
[Hoare Triple][hoare-triple]:

1. __given__ some sort of *pre-condition*
1. __when__ an *action* is performed
1. __then__ this should lead to some *post-condition*

In a regular function definition, one
would often verify that the pre-
and post-conditions are valid and 
the program is left in a valid state --- 
especially when applying 
[defensive programming][defensive-programming].
If the program ended up in an invalid state,
something exceptional happened and an exception
should be thrown.

When [using Cucumber][arothuis-cucumber] 
these three steps are often
indicated using the `Given`, `When`, `Then` keywords.
Although more code-oriented testing frameworks do
not have these keywords,
the steps can be emphasized through the structuring
of a test case (i.e. by applying adequate whitespace).
These steps have become known as the Three A's: 
[Act, Arrange, Assert][act-arrange-assert].

1. *Arrange* preconditions and inputs
1. *Act* on the system under test
1. *Assert* that the expected results have occured

## Keep Test Cases Small
Another way to improve our tests is to keep each
test case small. Just like our implementation code.
A guiding principle often found useful is to
only use the assertions needed to
verify a certain aspect of the unit's desired
behaviour. This closely aligns with the
[Single Responsibility Principle (SRP)]
[single-responsibility-principle] of
[SOLID][solid] fame: it applies to tests too!

Additionally, reduce the amount of 
arrangement code. If we need a lot of 
setup code, we might want to put this
in a separate setup function, leverage the
power of test doubles or refactor 
the unit under test if its implementation is 
too complex to test.

## Be Specific
By limiting the assertions to what is necessary
to verify a single aspect of a unit, we automatically
end up with a lot of specific test cases.
That is a good thing! As every test case requires
its own title or description, each assertion 
(verifying the behaviours of the unit) is
documented in the test, forming an
example-based specification of functionality.

Another benefit of fine-grained test cases
is that they make our tests less brittle:
changes to functionality will only affect
those tests that assert that functionality.
That is, unless it's in the set-up or tear-down of
a unit under test or when we are dealing with
the same test shape, but with varying
inputs and outputs. In those cases, 
it is a good time to refactor the tests!

## Refactor the Tests
Like our implementation code, refactoring
our test code is a good idea for
keeping understandability 
and maintainability in check.
We can even use
the reusability and composability patterns
we use to create maintainable implementation
code!

### Set Up and Tear Down
Setup code is the code needed to *arrange*
everything needed for executing a test. 
Often, this is code pertaining
to the construction and/or configuration
of the unit under test. This code can be
extracted as a function or (private) method
call so it can be reused where needed, even
with differing parameters. Making the resulting
test code more [DRY][dry]. An alternative, when
parameters are not necessary, are lifecycle
hooks like [JUnit's][junit-before-each] `@BeforeEach`, 
[Mocha's][mocha-hooks] `beforeEach()` 
and [Python's][python-hooks] `setUp()`.
The same applies for tear down code: code needed
to put the system under test back in a neutral,
testable state. Test runners often have hooks for
those states as well. 
[JUnit][junit-after-each] has `@AfterEach`,
[Mocha][mocha-hooks] `afterEach()` and 
[Python][python-hooks] `tearDown()`.

### Testing with Data
If the shape of test cases are similar but their
inputs and outputs vary, parameterized tests can be used.
This is often the case when multiple examples are
given for a single desired behavior.

Parameterized tests are methods that accept *parameters* 
in order to perform the test case with given test data.
For this, it uses can use a data source or a simple method
representing a test table with, per test case, the inputs
and outputs required to verify the desired behaviors.
In JUnit, we can use 
the [`@ParameterizedTest`][junit-parameterized-tests] annotation.
In [Cucumber][cucumber-outlines], 
`Scenario Outlines` and `Examples`
would be used for this.

As an alternative to a testing table, 
a copy of recorded anonymized production data 
can be used as a representative data set for testing.
Keep in mind, however, that this needs to be updated 
whenever functionality changes and the data source 
therefore needs to be offered in a maintainable fashion.

When doing testing against a database, 
[fixtures][fixtures] can be loaded in
the test database. Note that this kind of test
is not really considered a unit test, but rather
an integration, system or end-to-end test as it
spans multiple units and/or layers of the application
and even interacts with an external system (a database)!

Make sure fixtures are cleared up
after the test, for example by using a tear down function or hook.
But think long and hard if it is what we need
as we couple our tests to example production data
that might change as a result of evolving functionality
or database structure.

# Is Unit Testing Enough?
Although unit tests are a great starting point
for verifying the functionality of the units
in our system, additional measures are needed
to increase confidence in the implemented
functionality.

## Test Coverage
One way of measuring whether we have tested enough is
[code coverage][code-coverage].
Although 100% coverage is not always necessary, 
it is achievable through unit testing
(especially when working in a test-first fashion).
However, this does *not* imply that all of our implementation
code is thoroughly tested.
It merely means that all of our code is touched during testing.

### Statement versus branch coverage

### Mutation tests
In order to gain insight in the quality of our
tests, we can employ [mutation tests][mutation-tests].
In general, mutation tests mutate our implementation
code to see which tests break and which do not.
Typically, we want our tests to break when functionality
is altered. I might blog about mutation tests in a
later post.

## Test Scope
So, we have 100% coverage and our mutation tests
indicate that this coverage is reliable. Surely,
we can trust our software functions as expected?
No. I think we can trust that our units work correctly
--- in isolation.
By definition, unit tests do not verify that the
system as a whole behaves as we would like. They
do not even assert whether two or more units play
nice within the system or whether (groups of units) 
integrate well with external pieces of code, 
such as databases, messaging systems and the file system.

We can increase confidence by introducing *additional* tests,
focused on the functionality of groups of units 
([component tests][component-test]),
the integration of services between layers 
([internal integration tests][internal-integration-test]),
or the integration of our code with external systems 
([external integration tests][external-integration-test]),
the headless system as a whole 
([system][system-test] or [contract tests][contract-test])
or the system from the front-end to the back-end 
and back again ([end-to-end tests][end-to-end-test]).
This is not an exhaustive list and the terms and boundaries
are not as crystal-clear in practice.
As part of our [testing strategy][testing-strategy],
our application's requirements
and our architecture will dictate at which scopes to test.
This is a topic for another post.

A balanced test strategy typically results 
in a mix of tests at varying scopes.
But how much do we need? It depends, but generally we
want a lot of isolated and fast tests, and less
coupled and slow tests, because coupled and slow tests
are costly as they are more difficult to maintain
and widen the feedback loop: it takes longer before faults are detected.
This is the key idea behind the [agile test pyramid][agile-test-pyramid],
which is a good guideline, but is [not set in stone][not-agile-test-pyramid].

## Collaborative Testing
Unit testing, integration testing, end-to-end testing,
100% coverage with assurance from mutation tests: 
we have it all. Surely, our functionality is as desired, right?
Not necessarily. We do not write software for ourselves.
Actually, we do not even code for the company that hired us.
We write software for the people that need it.

That is why not only we need to focus on building the thing
right, but also on building the right thing. We need to check
with our clients, customers, end-users what they want.
This is where collaborative specification strategies
like [Specification by Example (SbE)][sbe], 
[Example Mapping][example-mapping] and
[Behaviour-Driven Development (BDD)][bdd] come into play.

And still: specification is no guarantee for success.
The only way to be sure is to let the code run in its
ever-evolving environment. Let it crash and burn.
Let it be used in ways that we did not and could not
foresee. Let new requirements or 
altered specifications stem from new insights.
As we build software in collaboration with the world,
some functionality can only be tested in production.
Codify and document these behaviours in a next iteration
by adding tests on the relevant layers.
This is how we remain confident in an application
that grows in demand, features and complexity.

[example-guided-development-1]: https://medium.com/@JoshuaKerievsky/example-guided-a-brief-history-f004ca19a96
[example-guided-development-2]: https://cucumber.io/blog/example-guided-development/
[single-responsibility-principle]:
[solid]:
[test-principles]: https://integralpath.blogs.com/thinkingoutloud/2005/09/principles_of_t.html
[hoare-triple]: https://en.wikipedia.org/wiki/Hoare_logic#Hoare_triple
[defensive-programming]: https://en.wikipedia.org/wiki/Defensive_programming
[arothuis-cucumber]: /posts/behaviour-driven-e2e-testing/#the-feature
[act-arrange-assert]: http://wiki.c2.com/?ArrangeActAssert
[dry]: https://en.wikipedia.org/wiki/Don%27t_repeat_yourself
[junit-before-each]: https://junit.org/junit5/docs/5.1.0/api/org/junit/jupiter/api/BeforeEach.html
[junit-after-each]: https://junit.org/junit5/docs/5.1.0/api/org/junit/jupiter/api/AfterEach.html
[mocha-hooks]: https://mochajs.org/#hooks
[python-hooks]: https://docs.python.org/3/library/unittest.html#organizing-test-code
[junit-parameterized-tests]: https://www.baeldung.com/parameterized-tests-junit-5
[cucumber-outlines]: https://cucumber.io/docs/gherkin/reference/#scenario-outline
[fixtures]: https://matthiasnoback.nl/2018/07/about-fixtures/
[code-coverage]:
[component-test]:
[internal-integration-test]: 
[external-integration-test]:
[system-test]:
[contract-test]:
[end-to-end-test]: /posts/behaviour-driven-e2e-testing
[testing-strategy]:
[agile-test-pyramid]: /posts/behaviour-driven-e2e-testing#agile-test-pyramid
[not-agile-test-pyramid]:
[sbe]:
[example-mapping]:
[bdd]: