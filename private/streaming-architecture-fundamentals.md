---
draft: true
title: "Streaming Architecture Fundamentals"
subtitle: "Combining Familiar Concepts"
series: "streaming-architecture"
summary: "
    Streaming architectures model the world as persistent streams of data.
    In this post, we explore how streams can provide certainty 
    in the uncertain world of distributed systems.
"
date: "2017-11-26"
tags: ["streaming", "reactive systems", "dataflow programming"]
---

"Πάντα ῥεῖ", or "everything flows", 
is a saying often attributed to the pre-Socratic philosopher [Heraclitus][heraclitus].
The world is in constant motion and is ever-changing over time.
The only thing of which one can be certain is that something happened at a certain time and place.
Streaming architectures follow this idea by modelling the world as streams of data.

In this post, we explore how persistent streams can provide certainty in the [uncertain world
of distributed systems][fallacies-of-distributed-computing]. 

# 1. Streaming Data
A *stream* is a continuous sequence of data describing change rather than the result of change.
A streaming service can therefore be as simple as a wrapper around an unbounded sequence of data, 
a ["dumb pipe" between "smart endpoints"][smart-endpoints-dumb-pipes].

The fundamental idea behind a streaming architecture is 
that streams are the centerpiece of the entire design: 
all data is in motion. 
By observing the stream of data, either in real-time or after-the-fact, 
decisions can be made and a model of the data can be formed (data in rest).

Whether it is for the integration between services, communication within a service, 
or for data integration and processing; 
streams holistically form the source of truth for the application. 

## 1.1. Event Streams
Because a data-point in a stream is often related to a fixed point in time, 
it is a natural conceptual fit to model them as events. 
When something has happened in an up-stream system (producer or source), an event has occurred. 
A down-stream system (consumer or sink) can then react to this event as it sees fit. 
The consumer can choose to ignore the event, process it and produce a new event, 
or use it to determine, transform and materialize its state in the form of a view or projection.

Characteristic of this event-driven architectural style is that it allows for decoupling between
producers and consumers, granting them autonomy. In fact, the producer is only coupled to a consumer 
through an intermediary streaming service and by means of the events produced.

## 1.2. Writing History ...
A stream of events can be modelled as a [log][log-data-structure],
an "append-only, totally-ordered sequence of records ordered by time".
Events happen in a sequence that can be determined through a concept of time that
is homogenously and uniformly applicable to each event in the sequence.
Furthermore, because one cannot rewrite history, events can only be added to the log 
and the events themselves should be [immutable][immutability]

Events can be persisted, not only for record-keeping (i.e. audit logs), 
but also as a resilience mechanism for dealing with failures.
Recording events provides the option of replays, 
which helps consumers receiving an event [at least once][at-least-once-delivery].
Furthermore, replayability can allow testing the impact of changes to 
common behavior by replaying a production event log in an acceptance environment.
This extreme form of temporal decoupling has its benefits, 
but requires consumers to be [idempotent][idempotent-consumers] 
regarding the past, present and future.

## 1.3. ... by Learning from History
Deducing state based on a (persisted) event log as the source of truth is known 
as [event sourcing][event-sourcing] to (enterprise) software architects. 
This way, events can be [reliably published][event-sourcing-pattern] when state changes occur. 

Because new state is based on an aggregation of the previous state and a transformation to apply, 
event sourcing can be generalized conceptually as a
[left fold][left-fold] ([try it][haskell-foldl]):
```haskell
currentBalance = foldl (+) initialBalance [receivedSalary, paidRent, paidGroceries]
  where initialBalance = 0
        receivedSalary = 2000
        paidRent = -1000
        paidGroceries = -500
        
-- Returns: 500
```

Although most applications register only the "current" state in some sort of database,
persistence of transformations to derive state from is not a novel concept: 
version control systems like Git use a [commit log][commit-log], 
and databases often rely on an [internal transaction log][db-transaction-log].
But even before software ate the world, people have been using logs to derive a certain state from. 
For ages, historians have recorded timelines of historic events,
 accountants have utilized ledgers for bookkeeping,
  and jurists have registered addenda for changes in law.

# 2. Reactive Systems
A streaming architecture can form the basis for a [reactive system](reactive-manifesto); 
one that, besides being message-driven, is responsive, resilient and elastic. 

Decoupling a consumer from a producer allows selective optimization
and picking the right tool for the job (i.e. read-optimized versus write-optimized persistence), 
leveraging principles as [Command Query Responsibility Segregation][cqrs] (CQRS).
This way, consumers can offer the most responsiveness where it is needed.

A streaming architecture has some resilient tendencies by default thanks to its decoupled nature. First of all, changes typically only affect one service at a time, making deployments flexible. Secondly, a failure in a producer or consumer will not bring down the entire system. Finally, in the event of a failure, consumers are able to continue processing from where they last were.
Furthermore, extra resilience can be found when services are able to be spatially decoupled using containment and replication.

An elastic system stays responsive under varying workload. 
This is possible in streaming architectures through operational or infrastructural automation and the decoupled nature of services.
Intelligent rate-limiting or back-pressure techniques can be applied on the streaming service to make sure producers and consumers keep up with eachother. 
Also, the [competing consumers pattern][competing-consumers] can be applied. 
Furthermore, if services are replicable, auto-scaling can help making a system more elastic.

## 2.1. Reactive Programming
* Reactive Programming is something else!
* Dataflow

# 3. Distributed Systems
In a streaming architecture, streams form the single source of truth of which
all other state is derrived and allow decoupling and isolation of state between services.
Given that the streaming service can be distributed itself, this means it can form the basis of a [Shared Nothing Architecture](shared-nothing), beloved by microservice enthousiasts and the like.

## 3.1. Capturing CAP
http://martin.kleppmann.com/2015/05/11/please-stop-calling-databases-cp-or-ap.html
https://www.infoq.com/articles/cap-twelve-years-later-how-the-rules-have-changed
http://robertgreiner.com/2014/08/cap-theorem-revisited/

## 3.2. Consistency model: ACID or BASE
https://www.johndcook.com/blog/2009/07/06/brewer-cap-theorem-base/
https://neo4j.com/blog/acid-vs-base-consistency-models-explained/

* Asynchronous message passing, ACID, BASE
* Strong consistency (ACID) is not always necessary

## 3.3. Distributed Ledger Technologies
* https://blockgeeks.com/guides/what-is-blockchain-technology/
* https://www.i-scoop.eu/blockchain-distributed-ledger-technology/
* http://hackingdistributed.com/2016/03/01/bitcoin-guarantees-strong-not-eventual-consistency/
* https://due.com/blog/bitcoin-consistent-not-eventually-consistent/
* https://www.infoq.com/news/2017/04/blockchain-cap-theorem
* State-machine replication.
* DLTs are NOT log-based, but Merkle tree based.

# 4. Challenges
## 4.1. Learning Curve
### 4.1.1. Distributed Systems and Eventual Consistency
Actor model.
Consensus protocols.
(Lack of) Delivery Guarantuees.

### 4.1.2. Temporal Decoupling and Idempotent Consumers
Pure functions.
Immutability.
Snapshots.

### 4.1.3. Programming Paradigms
Dataflow programming.
Functional programming.
Reactive programming.
Actor-based programming.

## 4.2. Changing Requirements and Domain Model
Versioning of events.

## 4.3. Operational complexity
## 4.3.1. Traceability and Monitoring
## 4.3.2. Hardware Requirements
* When relying on stream replays, it can be quite intensive to replay each stream from the beginning. Snapshot strategy.
* Although more flexible in CPU and Memory requirements (back pressure, process throttling), 
disk space strategy is needed. Compaction.

## 5. In closing

# Sources
* https://medium.com/@viktorklang/reactive-streams-1-0-0-interview-faaca2c00bec#.ckcwc9o10
* https://www.reactivemanifesto.org/glossary
* https://www.oreilly.com/ideas/the-world-beyond-batch-streaming-101
* Pat Helland, “Data on the Outside versus Data on the Inside,” Proceedings of 2005 CIDR Conference, Asilomar, California, http://cidrdb.org/cidr2005/papers/P12.pdf.
* Four real world streaming application architectures, https://www.youtube.com/watch?v=1Unqjy8uVaY
    
[heraclitus]: https://en.wikipedia.org/wiki/Heraclitus#Panta_rhei.2C_.22everything_flows.22
[fallacies-of-distributed-computing]: https://en.wikipedia.org/wiki/Fallacies_of_distributed_computing
[smart-endpoints-dumb-pipes]: https://martinfowler.com/articles/microservices.html#SmartEndpointsAndDumbPipes
[log-data-structure]: https://engineering.linkedin.com/distributed-systems/log-what-every-software-engineer-should-know-about-real-time-datas-unifying
[immutability]: https://www.ctl.io/developers/blog/post/immutability
[idempotent-consumers]: http://blog.jonathanoliver.com/idempotency-patterns/
[at-least-once-delivery]: http://www.cloudcomputingpatterns.org/at_least_once_delivery/
[commit-log]: https://git-scm.com/book/tr/v2/Git-Basics-Viewing-the-Commit-History
[db-transaction-log]: https://docs.microsoft.com/en-us/sql/relational-databases/logs/the-transaction-log-sql-server
[event-sourcing]: https://eventstore.org/docs/introduction/4.0.2/event-sourcing-basics/
[event-sourcing-pattern]: http://microservices.io/patterns/data/event-sourcing.html
[left-fold]: https://en.wikipedia.org/wiki/Fold_(higher-order_function)
[haskell-foldl]: https://repl.it/@arothuis/haskell-foldl
[reactive-manifesto]: https://www.reactivemanifesto.org/
[cqrs]: https://martinfowler.com/bliki/CQRS.html
[competing-consumers]: https://docs.microsoft.com/en-us/azure/architecture/patterns/competing-consumers
[shared-nothing]: https://en.wikipedia.org/wiki/Shared_nothing_architecture